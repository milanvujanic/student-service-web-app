package com.milan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.milan.springboot.entities.Questionnaire;
import com.milan.springboot.services.QuestionnaireService;

@Controller
public class QuestionnaireController {
	
	@Autowired
	private QuestionnaireService questionnaireService;
	
	@RequestMapping("addQuestionnaireConfirmation")
	public ModelAndView addQuestionnaireConfirmation(@RequestParam("teacherUsername") String teacherUsername, 
			@RequestParam("teacherName") String teacherName, 
			@RequestParam("teacherSurname") String teacherSurname, 
			@RequestParam("teacherCategoryDescription") String teacherCategoryDescription, 
			@RequestParam("selectedSubjectQuestionnaire") String selectedSubjectQuestionnaire, 
			ModelAndView modelAndView) {
		
		modelAndView.addObject("teacherUsername", teacherUsername);
		modelAndView.addObject("teacherName", teacherName);
		modelAndView.addObject("teacherSurname", teacherSurname);
		modelAndView.addObject("teacherCategoryDescription", teacherCategoryDescription);
		modelAndView.addObject("selectedSubjectQuestionnaire", selectedSubjectQuestionnaire);
		
		Questionnaire questionnaire = questionnaireService.addQuestionnaire(selectedSubjectQuestionnaire);
		if (questionnaire != null) {
			modelAndView.setViewName("teacher/addQuestionnaireConfirmation");
		} else {
			modelAndView.setViewName("teacher/addQuestionnaireError");
		}
		
		modelAndView.addObject("questionnaireId", questionnaire.getQuestionnaireId());
				
		return modelAndView;
	}
	
}
