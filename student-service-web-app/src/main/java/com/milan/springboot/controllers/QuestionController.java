package com.milan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.milan.springboot.entities.Question;
import com.milan.springboot.services.QuestionService;

@Controller
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	
	@RequestMapping("addQuestion")
	public ModelAndView addQuestion(@RequestParam("teacherUsername") String teacherUsername, 
			@RequestParam("teacherName") String teacherName, 
			@RequestParam("teacherSurname") String teacherSurname, 
			@RequestParam("teacherCategoryDescription") String teacherCategoryDescription, 
			@RequestParam("selectedSubjectQuestionnaire") String selectedSubjectQuestionnaire, 
			@RequestParam("questionnaireId") String questionnaireId, ModelAndView modelAndView) {
		modelAndView.addObject("teacherUsername", teacherUsername);
		modelAndView.addObject("teacherName", teacherName);
		modelAndView.addObject("teacherSurname", teacherSurname);
		modelAndView.addObject("teacherCategoryDescription", teacherCategoryDescription);
		modelAndView.addObject("selectedSubjectQuestionnaire", selectedSubjectQuestionnaire);
		modelAndView.addObject("questionnaireId", questionnaireId);
				
		modelAndView.setViewName("teacher/addQuestion");
		
		return modelAndView;
	}
	
	@RequestMapping("addQuestionConfirmation")
	public ModelAndView addQuestionConfirmation(@RequestParam("teacherUsername") String teacherUsername, 
			@RequestParam("teacherName") String teacherName, 
			@RequestParam("teacherSurname") String teacherSurname, 
			@RequestParam("teacherCategoryDescription") String teacherCategoryDescription, 
			@RequestParam("selectedSubjectQuestionnaire") String selectedSubjectQuestionnaire, 
			@RequestParam("questionnaireId") String questionnaireId,
			@RequestParam("selectedQuestionType") String selectedQuestionType, 
			@RequestParam("question_text") String question_text,
			@RequestParam("answer_1") String answer_1,
			@RequestParam("answer_2") String answer_2,
			@RequestParam("answer_3") String answer_3,
			@RequestParam("answer_4") String answer_4,
			@RequestParam("answer_5") String answer_5,
			ModelAndView modelAndView) {
		modelAndView.addObject("teacherUsername", teacherUsername);
		modelAndView.addObject("teacherName", teacherName);
		modelAndView.addObject("teacherSurname", teacherSurname);
		modelAndView.addObject("teacherCategoryDescription", teacherCategoryDescription);
		modelAndView.addObject("selectedSubjectQuestionnaire", selectedSubjectQuestionnaire);
		modelAndView.addObject("questionnaireId", questionnaireId);
		
		Question question = questionService.addQuestion(question_text, selectedQuestionType, questionnaireId,
				answer_1, answer_2, answer_3, answer_4, answer_5, null);
		
		modelAndView.setViewName("teacher/addQuestion");
	
		return modelAndView;
	}
}
