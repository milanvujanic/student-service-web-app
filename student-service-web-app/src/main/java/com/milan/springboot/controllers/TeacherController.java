package com.milan.springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.milan.springboot.entities.Subject;
import com.milan.springboot.services.TeacherService;

@Controller
public class TeacherController {
	
	@Autowired
	private TeacherService teacherService;
	
	@RequestMapping("indexTeacher")
	public ModelAndView goToIndexTeacher(@RequestParam("teacherUsername") String teacherUsername, 
			@RequestParam("teacherName") String teacherName, 
			@RequestParam("teacherSurname") String teacherSurname, 
			@RequestParam("teacherCategoryDescription") String teacherCategoryDescription, ModelAndView modelAndView) {
		modelAndView.addObject("teacherUsername", teacherUsername);
		modelAndView.addObject("teacherName", teacherName);
		modelAndView.addObject("teacherSurname", teacherSurname);
		modelAndView.addObject("teacherCategoryDescription", teacherCategoryDescription);
		modelAndView.setViewName("teacher/indexTeacher");
		
		return modelAndView;
	}

	@RequestMapping("addQuestionnaire")
	public ModelAndView addQuestionnaire(@RequestParam("teacherUsername") String teacherUsername, 
			@RequestParam("teacherName") String teacherName, 
			@RequestParam("teacherSurname") String teacherSurname, 
			@RequestParam("teacherCategoryDescription") String teacherCategoryDescription, ModelAndView modelAndView) {		
		List<Subject> teachersSubjects = teacherService.findTeachersSubjects(teacherUsername);
		modelAndView.addObject("teachersSubjects", teachersSubjects);
		modelAndView.addObject("teacherUsername", teacherUsername);
		modelAndView.addObject("teacherName", teacherName);
		modelAndView.addObject("teacherSurname", teacherSurname);
		modelAndView.addObject("teacherCategoryDescription", teacherCategoryDescription);
		modelAndView.setViewName("teacher/addQuestionnaire");
		
		return modelAndView;
	}	
}
