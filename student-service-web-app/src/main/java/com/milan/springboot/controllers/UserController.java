package com.milan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.milan.springboot.entities.LoginBean;
import com.milan.springboot.entities.Student;
import com.milan.springboot.entities.Teacher;
import com.milan.springboot.services.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="loginForm", method=RequestMethod.POST)
	public ModelAndView showUsers(@ModelAttribute("loginBean") LoginBean loginBean, ModelAndView modelAndView) {
		Object o = userService.findTeacherOrStudent(loginBean.getUsernameOrEmail(), loginBean.getPassword());
		
		if (o == null || loginBean.getUsernameOrEmail() == null || loginBean.getUsernameOrEmail().equals("")
				|| loginBean.getPassword() == null || loginBean.getPassword().equals("")) {
			modelAndView.setViewName("invalidLogin");
			return modelAndView;
		}
		
		if (o instanceof Teacher) {
			Teacher teacher = (Teacher) o;
			String teacherUsername = teacher.getTeacherUsername();
			String teacherName = teacher.getTeacherName();
			String teacherSurname = teacher.getTeacherSurname();
			String teacherCategoryDescription = teacher.getTeacherCategory().getTeacherCategoryDescription();
			modelAndView.addObject("teacherUsername", teacherUsername);
			modelAndView.addObject("teacherName", teacherName);
			modelAndView.addObject("teacherSurname", teacherSurname);
			modelAndView.addObject("teacherCategoryDescription", teacherCategoryDescription);
			modelAndView.setViewName("teacher/indexTeacher");
			return modelAndView;
		}
		
		Student student = (Student) o;
		modelAndView.addObject("student", student);
		modelAndView.setViewName("student/indexStudent");
		return modelAndView;
	}
	
	@RequestMapping("/invalidLogin")
	public String goToInvalidLoginPage() {
		return "invalidLogin";
	}	
	
}













