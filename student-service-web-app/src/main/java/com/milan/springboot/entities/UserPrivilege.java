package com.milan.springboot.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_privileges")
public class UserPrivilege {

	@Id
	@Column(name = "user_privilege_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer userPrivilegeId;

	@Column(name = "user_privilege_description")
	private String userPrivilegeDescription;
	
	@OneToMany(mappedBy="userPrivilege")
	private List<User> users;

	public Integer getUserPrivilegeId() {
		return userPrivilegeId;
	}

	public void setUserPrivilegeId(Integer userPrivilegeId) {
		this.userPrivilegeId = userPrivilegeId;
	}

	public String getUserPrivilegeDescription() {
		return userPrivilegeDescription;
	}

	public void setUserPrivilegeDescription(String userPrivilegeDescription) {
		this.userPrivilegeDescription = userPrivilegeDescription;
	}

}
