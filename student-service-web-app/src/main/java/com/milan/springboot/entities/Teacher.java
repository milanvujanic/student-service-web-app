package com.milan.springboot.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "teachers")
public class Teacher {

	@Id
	@Column(name = "teacher_username")
	private String teacherUsername;

	@NotNull
	@Column(name = "teacher_email")
	private String teacherEmail;

	@NotNull
	@Column(name = "teacher_password")
	private String teacherPassword;

	@NotNull
	@Column(name = "teacher_name")
	private String teacherName;

	@NotNull
	@Column(name = "teacher_surname")
	private String teacherSurname;

	@ManyToOne
	@JoinColumn(name = "teacher_category_id", insertable = false, updatable = false)
	private TeacherCategory teacherCategory;

	@ManyToMany
	@JoinTable(name = "teachers_subjects", joinColumns = @JoinColumn(name = "teacher_username"), inverseJoinColumns = @JoinColumn(name = "subject_id"))
	private List<Subject> subjects;

	public String getTeacherEmail() {
		return teacherEmail;
	}

	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}

	public String getTeacherPassword() {
		return teacherPassword;
	}

	public void setTeacherPassword(String teacherPassword) {
		this.teacherPassword = teacherPassword;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTeacherSurname() {
		return teacherSurname;
	}

	public void setTeacherSurname(String teacherSurname) {
		this.teacherSurname = teacherSurname;
	}

	public String getTeacherUsername() {
		return teacherUsername;
	}

	public void setTeacherUsername(String teacherUsername) {
		this.teacherUsername = teacherUsername;
	}

	public TeacherCategory getTeacherCategory() {
		return teacherCategory;
	}

	public void setTeacherCategory(TeacherCategory teacherCategory) {
		this.teacherCategory = teacherCategory;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	@Override
	public String toString() {
		return "Teacher [teacherUsername=" + teacherUsername + ", teacherEmail=" + teacherEmail + ", teacherPassword="
				+ teacherPassword + ", teacherName=" + teacherName + ", teacherSurname=" + teacherSurname
				+ ", teacherCategory=" + teacherCategory + "]";
	}

}
