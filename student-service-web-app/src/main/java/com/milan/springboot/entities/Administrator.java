package com.milan.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "administrators")
public class Administrator {

	@Id
	@Column(name = "administrator_username")
	private String administratorUsername;

	@Column(name = "administrator_email")
	private String administratorEmail;

	@Column(name = "administrator_password")
	private String administratorPassword;

	@Column(name = "administrator_name")
	private String administratorName;

	@Column(name = "administrator_surname")
	private String administratorSurname;

	public String getAdministratorUsername() {
		return administratorUsername;
	}

	public void setAdministratorUsername(String administratorUsername) {
		this.administratorUsername = administratorUsername;
	}

	public String getAdministratorEmail() {
		return administratorEmail;
	}

	public void setAdministratorEmail(String administratorEmail) {
		this.administratorEmail = administratorEmail;
	}

	public String getAdministratorPassword() {
		return administratorPassword;
	}

	public void setAdministratorPassword(String administratorPassword) {
		this.administratorPassword = administratorPassword;
	}

	public String getAdministratorName() {
		return administratorName;
	}

	public void setAdministratorName(String administratorName) {
		this.administratorName = administratorName;
	}

	public String getAdministratorSurname() {
		return administratorSurname;
	}

	public void setAdministratorSurname(String administratorSurname) {
		this.administratorSurname = administratorSurname;
	}

}
