package com.milan.springboot.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "questionnaires")
public class Questionnaire {

	@Id
	@Column(name = "questionnaire_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer questionnaireId;

	@Column(name = "questionnaire_title")
	private String questionnaireTitle;

	@ManyToMany
	@JoinTable(name = "subjects_questionnaires", joinColumns = @JoinColumn(name = "questionnaire_id"), inverseJoinColumns = @JoinColumn(name = "subject_id"))
	private List<Subject> questionnaireSubjects;

	@ManyToMany
	@JoinTable(name = "questionnaires_questions", joinColumns = @JoinColumn(name = "questionnaire_id"), inverseJoinColumns = @JoinColumn(name = "question_id"))
	private List<Question> questions;

	public Questionnaire() {
		super();
	}

	public Questionnaire(String questionnaireTitle) {
		super();
		this.questionnaireTitle = questionnaireTitle;
	}

	public Integer getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public String getQuestionnaireTitle() {
		return questionnaireTitle;
	}

	public void setQuestionnaireTitle(String questionnaireTitle) {
		this.questionnaireTitle = questionnaireTitle;
	}

	public List<Subject> getQuestionnaireSubjects() {
		return questionnaireSubjects;
	}

	public void setQuestionnaireSubjects(List<Subject> questionnaireSubjects) {
		this.questionnaireSubjects = questionnaireSubjects;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

}
