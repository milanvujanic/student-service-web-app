package com.milan.springboot.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "teacher_category")
public class TeacherCategory {

	@Id
	@Column(name = "teacher_category_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer teacherCategoryId;

	@Column(name = "teacher_category_description")
	private String teacherCategoryDescription;

	@OneToMany(mappedBy = "teacherCategory")
	private List<Teacher> teachers;

	public Integer getTeacherCategoryId() {
		return teacherCategoryId;
	}

	public void setTeacherCategoryId(Integer teacherCategoryId) {
		this.teacherCategoryId = teacherCategoryId;
	}

	public String getTeacherCategoryDescription() {
		String firstLetterCapitalized = teacherCategoryDescription.substring(0, 1).toUpperCase();
		String withoutFirstLetter = teacherCategoryDescription.substring(1);
		return firstLetterCapitalized + withoutFirstLetter;
	}

	public void setTeacherCategoryDescription(String teacherCategoryDescription) {
		this.teacherCategoryDescription = teacherCategoryDescription;
	}

	@Override
	public String toString() {
		return "TeacherCategory [teacherCategoryId=" + teacherCategoryId + ", teacherCategoryDescription="
				+ teacherCategoryDescription + "]";
	}

}
