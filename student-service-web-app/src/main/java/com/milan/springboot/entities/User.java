package com.milan.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class User {

	@Id
	@Column(name = "user_username")
	private String userUsername;

	@NotNull
	@Size(max = 90)
	@Column(name = "user_email")
	private String userEmail;

	@NotNull
	@Size(max = 128)
	@Column(name = "user_password")
	private String userPassword;

	@NotNull
	@Column(name = "user_privilege_id")
	private Integer userPrivilegeId;

	@ManyToOne
	@JoinColumn(name = "user_privilege_id", insertable = false, updatable = false)
	private UserPrivilege userPrivilege;

	@OneToOne
	@JoinColumn(name = "student_username", insertable = false, updatable = false)
	private Student student;

	@OneToOne
	@JoinColumn(name = "teacher_username", insertable = false, updatable = false)
	private Teacher teacher;

	@OneToOne
	@JoinColumn(name = "superadministrator_username", insertable = false, updatable = false)
	private Superadministrator superadministrator;

	@OneToOne
	@JoinColumn(name = "administrator_username", insertable = false, updatable = false)
	private Administrator administrator;

	public String getUserUsername() {
		return userUsername;
	}

	public void setUserUsername(String userUsername) {
		this.userUsername = userUsername;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Integer getUserPrivilegeId() {
		return userPrivilegeId;
	}

	public void setUserPrivilegeId(Integer userPrivilegeId) {
		this.userPrivilegeId = userPrivilegeId;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}	

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "User [userUsername=" + userUsername + ", userEmail=" + userEmail + ", userPassword=" + userPassword
				+ ", userPrivilegeId=" + userPrivilegeId + "]";
	}

}
