package com.milan.springboot.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "question_types")
public class QuestionType {

	@Id
	@Column(name = "question_type_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer questionTypeId;

	@Column(name = "question_type_description")
	private String questionTypeDescription;

	@OneToMany(mappedBy = "questionType")
	private List<Question> questions;

	public Integer getQuestionTypeId() {
		return questionTypeId;
	}

	public void setQuestionTypeId(Integer questionTypeId) {
		this.questionTypeId = questionTypeId;
	}

	public String getQuestionTypeDescription() {
		return questionTypeDescription;
	}

	public void setQuestionTypeDescription(String questionTypeDescription) {
		this.questionTypeDescription = questionTypeDescription;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

}
