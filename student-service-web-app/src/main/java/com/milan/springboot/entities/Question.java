package com.milan.springboot.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "questions")
public class Question {

	@Id
	@Column(name = "question_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer questionId;

	@Column(name = "question_text")
	private String questionText;

	@Column(name = "answer_1")
	private String answer1;

	@Column(name = "answer_2")
	private String answer2;

	@Column(name = "answer_3")
	private String answer3;

	@Column(name = "answer_4")
	private String answer4;

	@Column(name = "answer_5")
	private String answer5;

	@Column(name = "answers_correct")
	private String answersCorrect;

	@ManyToOne
	@JoinColumn(name = "question_type_id", insertable = false, updatable = false)
	private QuestionType questionType;

	@ManyToMany
	@JoinTable(name = "questionnaires_questions", joinColumns = @JoinColumn(name = "subject_id"), inverseJoinColumns = @JoinColumn(name = "questionnaire_id"))
	private List<Questionnaire> questionnaires;

	public Question() {
		super();
	}

	public Question(String questionText, String answer1, String answer2, String answer3, String answer4, String answer5,
			String answersCorrect) {
		super();
		this.questionText = questionText;
		this.answer1 = answer1;
		this.answer2 = answer2;
		this.answer3 = answer3;
		this.answer4 = answer4;
		this.answer5 = answer5;
		this.answersCorrect = answersCorrect;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public List<Questionnaire> getQuestionnaires() {
		return questionnaires;
	}

	public void setQuestionnaires(List<Questionnaire> questionnaires) {
		this.questionnaires = questionnaires;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	public String getAnswer4() {
		return answer4;
	}

	public void setAnswer4(String answer4) {
		this.answer4 = answer4;
	}

	public String getAnswer5() {
		return answer5;
	}

	public void setAnswer5(String answer5) {
		this.answer5 = answer5;
	}

	public String getAnswersCorrect() {
		return answersCorrect;
	}

	public void setAnswersCorrect(String answersCorrect) {
		this.answersCorrect = answersCorrect;
	}

}
