package com.milan.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "superadministrators")
public class Superadministrator {

	@Id
	@Column(name = "superadministrator_username")
	private String superadministratorUsername;

	@Column(name = "superadministrator_email")
	private String superadministratorEmail;

	@Column(name = "superadministrator_password")
	private String superadministratorPassword;

	@Column(name = "superadministrator_name")
	private String superadministratorName;

	@Column(name = "superadministrator_surname")
	private String superadministratorSurname;

	public String getSuperadministratorUsername() {
		return superadministratorUsername;
	}

	public void setSuperadministratorUsername(String superadministratorUsername) {
		this.superadministratorUsername = superadministratorUsername;
	}

	public String getSuperadministratorEmail() {
		return superadministratorEmail;
	}

	public void setSuperadministratorEmail(String superadministratorEmail) {
		this.superadministratorEmail = superadministratorEmail;
	}

	public String getSuperadministratorPassword() {
		return superadministratorPassword;
	}

	public void setSuperadministratorPassword(String superadministratorPassword) {
		this.superadministratorPassword = superadministratorPassword;
	}

	public String getSuperadministratorName() {
		return superadministratorName;
	}

	public void setSuperadministratorName(String superadministratorName) {
		this.superadministratorName = superadministratorName;
	}

	public String getSuperadministratorSurname() {
		return superadministratorSurname;
	}

	public void setSuperadministratorSurname(String superadministratorSurname) {
		this.superadministratorSurname = superadministratorSurname;
	}

}
