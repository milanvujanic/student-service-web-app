package com.milan.springboot.repositories;

import org.springframework.data.repository.CrudRepository;

import com.milan.springboot.entities.Question;

public interface QuestionRepository extends CrudRepository<Question, Integer> {

}
