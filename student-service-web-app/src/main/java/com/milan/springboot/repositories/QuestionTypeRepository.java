package com.milan.springboot.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.milan.springboot.entities.QuestionType;

public interface QuestionTypeRepository extends CrudRepository<QuestionType, Integer>{
	List<QuestionType> findByQuestionTypeId(Integer questionTypeId);
}
