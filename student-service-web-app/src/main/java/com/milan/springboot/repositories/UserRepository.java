package com.milan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.milan.springboot.entities.User;

public interface UserRepository extends JpaRepository<User, String> {
	User findByUserUsernameAndUserPassword(String userUsername,String userPassword);
	User findByUserEmailAndUserPassword(String userEmail, String userPassword);	
	
}
