package com.milan.springboot.repositories;

import org.springframework.data.repository.CrudRepository;

import com.milan.springboot.entities.TeacherCategory;

public interface TeacherCategoryRepository extends CrudRepository<TeacherCategory, Integer> {

}
