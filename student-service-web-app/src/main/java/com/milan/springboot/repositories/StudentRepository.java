package com.milan.springboot.repositories;

import org.springframework.data.repository.CrudRepository;

import com.milan.springboot.entities.Student;

public interface StudentRepository extends CrudRepository<Student, String> {

}
