package com.milan.springboot.repositories;

import org.springframework.data.repository.CrudRepository;

import com.milan.springboot.entities.Teacher;

public interface TeacherRepository extends CrudRepository<Teacher, String> {
	Teacher findByTeacherUsername(String teacherUsername);	
}
