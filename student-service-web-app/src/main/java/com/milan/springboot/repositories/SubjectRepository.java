package com.milan.springboot.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.milan.springboot.entities.Subject;

public interface SubjectRepository extends CrudRepository<Subject, Integer> {
	List<Subject> findBySubjectTitle(String subjectTitle);
}
