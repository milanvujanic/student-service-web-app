package com.milan.springboot.repositories;

import org.springframework.data.repository.CrudRepository;

import com.milan.springboot.entities.UserPrivilege;

public interface UserPrivilegeRepository extends CrudRepository<UserPrivilege, Integer> {

}
