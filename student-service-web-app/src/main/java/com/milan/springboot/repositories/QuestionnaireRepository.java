package com.milan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.milan.springboot.entities.Questionnaire;

public interface QuestionnaireRepository extends JpaRepository<Questionnaire, Integer> {
	Questionnaire findByQuestionnaireId(Integer questionnaireId);
}
