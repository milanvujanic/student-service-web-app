package com.milan.springboot.services;

import java.util.List;

import com.milan.springboot.entities.QuestionType;

public interface QuestionTypeService {
	
	List<QuestionType> findQuestionType(Integer questionTypeId);
}
