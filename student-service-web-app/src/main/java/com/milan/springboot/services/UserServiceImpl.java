package com.milan.springboot.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milan.springboot.entities.Student;
import com.milan.springboot.entities.Teacher;
import com.milan.springboot.entities.User;
import com.milan.springboot.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	
	@Autowired	
	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public List<User> findAll() {
		List<User> users = (List<User>) userRepository.findAll();
		for (User user : users) {
			System.out.println(user);
		}
		return users;
	}

	@Override
	@Transactional
	public User find(String userUsernameOrEmail, String userPassword) {
		User user = userRepository.findByUserUsernameAndUserPassword(userUsernameOrEmail, userPassword);
		if (user != null) {
			return user;
		}
		
		user = userRepository.findByUserEmailAndUserPassword(userUsernameOrEmail, userPassword);
		return user;
	}

	@Override
	@Transactional
	public Object findTeacherOrStudent(String userUsernameOrEmail, String userPassword) {
		User user = find(userUsernameOrEmail, userPassword);
		if (user == null) {
			return null;
		}
		
		Teacher teacher = user.getTeacher();
		if(teacher!= null) {
			return teacher;
		}
		
		Student student = user.getStudent();
		return student;
	}
	
	
	

}
