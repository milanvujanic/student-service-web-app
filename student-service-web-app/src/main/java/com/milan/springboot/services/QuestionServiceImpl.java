package com.milan.springboot.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milan.springboot.entities.Question;
import com.milan.springboot.entities.QuestionType;
import com.milan.springboot.entities.Questionnaire;
import com.milan.springboot.repositories.QuestionRepository;

@Service
public class QuestionServiceImpl implements QuestionService {

	@Autowired
	private QuestionRepository questionRepository;
	
	@Autowired
	private QuestionnaireService questionnaireService;
	
	@Autowired
	private QuestionTypeService questionTypeService;
	
	@Override
	@Transactional
	public Question addQuestion(String questionText, String questionTypeIdd, String questionnaireId, 
			String answer_1, String answer_2, String answer_3, String answer_4,
			String answer_5, String answers_correct) {
		Integer questionTypeId = Integer.parseInt(questionTypeIdd);
		
		List<QuestionType> questionTypes = questionTypeService.findQuestionType(questionTypeId);
		QuestionType questionType = questionTypes.get(0); 
		Question question = questionRepository.save(new Question(questionText, answer_1, answer_2, answer_3, answer_4, answer_5, answers_correct));
		question.setQuestionType(questionType);
		
		List<Question> questions = new ArrayList<>();
		questions.add(question);	
				
		Questionnaire questionnaire = questionnaireService.findQuestionnaireById(questionnaireId);
		
		questionnaire.setQuestions(questions);
		
		return question;
	}
	

}
