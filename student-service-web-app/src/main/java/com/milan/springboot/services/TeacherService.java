package com.milan.springboot.services;

import java.util.List;

import com.milan.springboot.entities.Subject;
import com.milan.springboot.entities.Teacher;

public interface TeacherService {
	List<Subject> findTeachersSubjects(String teacherUsername);
	Teacher findByTeacherUsername(String teacherUsername);
}
