package com.milan.springboot.services;

import com.milan.springboot.entities.Question;

public interface QuestionService {
	public Question addQuestion(String questionText, String questionTypeId, String questionnaireId, String answer_1, String answer_2, String answer_3, String answer_4,
			String answer_5, String answers_correct);
}
