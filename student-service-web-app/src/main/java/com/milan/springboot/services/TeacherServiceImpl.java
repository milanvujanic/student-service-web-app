package com.milan.springboot.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milan.springboot.entities.Subject;
import com.milan.springboot.entities.Teacher;
import com.milan.springboot.repositories.TeacherRepository;

@Service
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	private TeacherRepository teacherRepository;
	
	@Override
	@Transactional
	public List<Subject> findTeachersSubjects(String teacherUsername) {
		Teacher teacher = teacherRepository.findByTeacherUsername(teacherUsername);
		List<Subject> teachersSubjects = teacher.getSubjects();
		
		return teachersSubjects;
	}

	@Override
	@Transactional
	public Teacher findByTeacherUsername(String teacherUsername) {
		Teacher teacher = teacherRepository.findByTeacherUsername(teacherUsername);
		
		return teacher;
	}

}
