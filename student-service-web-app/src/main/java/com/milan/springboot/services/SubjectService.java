package com.milan.springboot.services;

import java.util.List;

import com.milan.springboot.entities.Subject;

public interface SubjectService {
	
	List<Subject> findSubjects(String subjectTitle);
}
