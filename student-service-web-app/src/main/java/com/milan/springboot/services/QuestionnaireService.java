package com.milan.springboot.services;

import com.milan.springboot.entities.Questionnaire;

public interface QuestionnaireService {
	Questionnaire addQuestionnaire(String subjectTitle);
	Questionnaire findQuestionnaireById(String questionnaireId);
}
