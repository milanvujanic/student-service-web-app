package com.milan.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milan.springboot.entities.Subject;
import com.milan.springboot.repositories.SubjectRepository;

@Service
public class SubjectServiceImpl implements SubjectService {
	
	@Autowired
	private SubjectRepository subjectRepository;

	@Override
	public List<Subject> findSubjects(String subjectTitle) {
		List<Subject> subjects = subjectRepository.findBySubjectTitle(subjectTitle);
		return subjects;
	}

}
