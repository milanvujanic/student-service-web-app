package com.milan.springboot.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milan.springboot.entities.Questionnaire;
import com.milan.springboot.entities.Subject;
import com.milan.springboot.repositories.QuestionnaireRepository;
import com.milan.springboot.repositories.SubjectRepository;

@Service
public class QuestionnaireServiceImpl implements QuestionnaireService {

	@Autowired
	private QuestionnaireRepository questionnaireRepository;
	
	@Autowired
	private SubjectRepository subjectRepository;
	
	@Override
	@Transactional
	public Questionnaire addQuestionnaire(String subjectTitle) {		
		Questionnaire questionnaire = questionnaireRepository.save(new Questionnaire(subjectTitle));
		List<Subject> subjects = subjectRepository.findBySubjectTitle(subjectTitle);
		questionnaire.setQuestionnaireSubjects(subjects);
		
		return questionnaire;
	}

	@Override
	@Transactional
	public Questionnaire findQuestionnaireById(String questionnaireId) {
		Integer id = Integer.parseInt(questionnaireId);
		Questionnaire questionnaire = questionnaireRepository.findByQuestionnaireId(id);
		
		return questionnaire;
	}
	
	

}
