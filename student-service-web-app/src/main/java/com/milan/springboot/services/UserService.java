package com.milan.springboot.services;

import java.util.List;
import com.milan.springboot.entities.User;

public interface UserService {
	List<User> findAll();
	User find(String userUsernameOrEmail, String userPassword);
	Object findTeacherOrStudent(String userUsernameOrEmail, String userPassword);
}
