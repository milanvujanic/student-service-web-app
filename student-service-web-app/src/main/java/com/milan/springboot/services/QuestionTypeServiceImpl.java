package com.milan.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milan.springboot.entities.QuestionType;
import com.milan.springboot.repositories.QuestionTypeRepository;

@Service
public class QuestionTypeServiceImpl implements QuestionTypeService {

	@Autowired
	private QuestionTypeRepository questionTypeRepository;
	
	@Override
	public List<QuestionType> findQuestionType(Integer questionTypeId) {
		List<QuestionType> questionType = questionTypeRepository.findByQuestionTypeId(questionTypeId);
		
		return questionType;
	}

	

}
