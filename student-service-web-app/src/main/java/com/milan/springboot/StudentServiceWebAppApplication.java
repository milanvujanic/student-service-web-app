package com.milan.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentServiceWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentServiceWebAppApplication.class, args);
	}

}
